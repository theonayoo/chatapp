import Pusher from 'pusher-js/react-native';
import PusherConfig from './pusher.json';

export default () => {
    let options = {
        encrypted: false,
        key: PusherConfig.key,
        wsHost: PusherConfig.host_url,
        wsPort: PusherConfig.Port,
        // disableStats: true,
        // authEndpoint: AUTH_URL + 'broadcasting/auth',
        // forceTLS: false,
        // auth: {
        //     headers: {
        //         'Authorization': 'Bearer ' + token,
        //     },
        // }
    };
    return new Pusher(options.key, options);
};
